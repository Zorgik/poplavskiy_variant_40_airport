using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace poplavskiy_variant_40_airport
{
    class Plane
    {
        static int id { get; set; }
        public int Key { get; set; }
        public string Name { get; set; }
        string Category { get; set; }
        public int Plases { get; set; }
        public Plane(string name,int plases,string category)
        {
            this.Plases = plases;
            this.Category = category;
            this.Name = name;
            this.Key = id++;
        }
        public string Information()
        {
            string info = Key + ":" + Name + ":" + Plases + ":" + Category;
            return info;
        }
    }
    class Flight
    {
        static int id { get; set; }
        public int Key { get; set; }
        public int FlightNumber { get; set; }
        public string PlaneName { get; set; }
        public int FreePlases { get; set; }
        public string TimeToStart { get; set; }
        public int Price { get; set; }
        public Flight(int flightnumber,string planename,int plases,string timetostart,int price)
        {
            this.FreePlases = plases;
            this.TimeToStart = timetostart;
            this.PlaneName = planename;
            this.Key = id++;
            this.FlightNumber = flightnumber;
            this.Price = price;
        }
        public string Information()
        {
            string info = Key + ":" + FlightNumber + ":" + PlaneName + ":" + FreePlases + ":" + TimeToStart + ":" + Price;
            return info;
        }
    }
    class Ticket
    {
        static int id { get; set; }
        public int Key { get; set; }
        int CashboxNumber { get; set; }
        public int FlightNumber { get; set; }
        string Date { get; set; }
        string Time { get; set; }
        public Ticket(int cashboxnumber,int flightnumber,string date,string time)
        {
            this.Time = time;
            this.Date = date;
            this.Key = id++;
            this.FlightNumber = flightnumber;
            this.CashboxNumber = cashboxnumber;
        }
        public string Information()
        {
            string info = Key + ":" + CashboxNumber + ":" + FlightNumber + ":" + Date + ":" + Time;
            return info;
        }
    }
    class Program
    {
        static Random rnd = new Random();
        static void Main(string[] args)
        {
            string TicketPath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Ticket.txt";
            string FlightPath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Flight.txt";
            string PlanePath = @"/home/kalin/Documents/c#/Poplavskiy_Kursovaya_Variant_40_Airport/poplavskiy_variant_40_airport/bd/Plane.txt";
            List<Plane> PlaneList = new List<Plane>();
            List<Flight> FlightList = new List<Flight>();
            List<Ticket> TicketList = new List<Ticket>();
            using (StreamReader sr = new StreamReader(PlanePath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { ':' });
                    PlaneList.Add(new Plane(words[1],Convert.ToInt32(words[2]),words[3]));
                }
            }
            using (StreamReader sr = new StreamReader(FlightPath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { ':' });
                    FlightList.Add(new Flight(Convert.ToInt32(words[1]), words[2], Convert.ToInt32(words[3]), words[4], Convert.ToInt32(words[5]) ));
                }
            }
            using (StreamReader sr = new StreamReader(TicketPath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { ':' });
                    TicketList.Add(new Ticket(Convert.ToInt32(words[1]),Convert.ToInt32(words[2]),words[3],words[4]));
                }
            }
            bool end = true;
            while(end)
            {
                Console.WriteLine("1.Most expensive flight");
                Console.WriteLine("2.Are there free places for a given flight");
                Console.WriteLine("3.What flights are served by a given aircraft");
                Console.WriteLine("4.On which flights you can replace the plane");
                Console.WriteLine("5.Add flight");
                Console.WriteLine("6.Remove flight and tickets on this flight");
                Console.WriteLine("7.Exit\n\n\n");
                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        var MAXCOST = FlightList.Max(i => i.Price);
                        var selectOne = FlightList.Where(i => i.Price == MAXCOST).Select(i => i.Information());
                        foreach(var sel in selectOne)
                        {
                            Console.WriteLine("The most expensive flight is: " + sel + "\n");
                        }
                        break;
                    case 2:
                        foreach(Flight fli in FlightList)
                        {
                            Console.WriteLine(fli.Information());
                        }
                        Console.WriteLine("Enter flight number");
                        int GivenFlight = Convert.ToInt32(Console.ReadLine());
                        var selectTwo = from i in FlightList
                        where i.FreePlases > 0 && i.FlightNumber == GivenFlight
                        select i.FreePlases;
                        foreach(var sel in selectTwo)
                        {
                            Console.WriteLine("This flight have " + sel + " free plases");  
                        }
                        break;
                    case 3:
                        Console.WriteLine("Plane list");
                        foreach(Plane pl in PlaneList)
                        {
                            Console.WriteLine(pl.Name);
                        }
                        string plName = Console.ReadLine();               
                        var selectThree = from i in FlightList                
                                    where i.PlaneName == plName           
                                    select i.Information();
                        foreach(var sel in selectThree)
                        {
                            Console.WriteLine(sel);
                        }
                        break;
                    case 4:
                        var selectFour = from i in FlightList
                        where i.FreePlases < 150
                        select i.Information();
                        foreach(var sel in selectFour)
                        {
                            Console.WriteLine(sel);
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter flight number: ");
                        int Number = Convert.ToInt32(Console.ReadLine());
                        int planeID = 0;
                        foreach(Plane plane in PlaneList)
                        {
                            Console.WriteLine(plane.Name);
                            planeID = plane.Key;
                        }
                        Console.WriteLine("Enter plane name: ");
                        string MachineName = Console.ReadLine();
                        Console.WriteLine($"Enter free palses (but not anymore \"{PlaneList[planeID].Plases}\"): ");
                        int OpenPlases = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter time when flight start (for example 20-00): ");
                        string TimeToGo = Console.ReadLine();
                        Console.WriteLine("Enter price");
                        int Cost = Convert.ToInt32(Console.ReadLine());
                        FlightList.Add(new Flight(Number,MachineName,OpenPlases,TimeToGo,Cost));
                        using (StreamWriter sw = new StreamWriter(FlightPath,false,System.Text.Encoding.Default))
                        {
                            foreach(Flight fli in FlightList)
                            {
                                sw.WriteLine(fli.Information());
                            }
                        }
                        string[] days = new string[31]; 
                        for(int o = 0; o < 31; o++)
                        {
                            days[o] = Convert.ToString(o);
                        }
                        string day = days[rnd.Next(0,30)];    
                        for(int j = 0; j < OpenPlases; j++)
                        {
                            TicketList.Add(new Ticket(rnd.Next(0,20),Number,day,TimeToGo));
                        }
                        using (StreamWriter sw = new StreamWriter(TicketPath, false, System.Text.Encoding.Default))
                        {
                            foreach(Ticket t in TicketList)
                            {
                                sw.WriteLine(t.Information());
                            }
                        }
                        break;
                    case 6:
                        Console.WriteLine("How flight must be removed(enter id): ");
                        int rmElement = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            Console.WriteLine("Start to remve");
                            bool Tick = true;
                                for(int i = 0; i < TicketList.Count()-1;i++)
                                {
                                    if(TicketList[i].FlightNumber == FlightList[rmElement].FlightNumber)
                                    {
                                        TicketList.RemoveAt(i);
                                        i--;
                                    }
                                }
                            Console.WriteLine("Has been removed");
                            FlightList.RemoveAt(rmElement);
                            using (StreamWriter sw = new StreamWriter(FlightPath,false,System.Text.Encoding.Default))
                            {
                                foreach(Flight fli in FlightList)
                                {
                                    sw.WriteLine(fli.Information());
                                }
                            }
                            using (StreamWriter sw = new StreamWriter(TicketPath, false, System.Text.Encoding.Default))
                            {
                                foreach(Ticket t in TicketList)
                                {
                                    sw.WriteLine(t.Information());
                                }
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Sorri");
                        }
                        break;
                    case 7:
                        end = false;
                        break;
                }
            }
        }
    }
}
